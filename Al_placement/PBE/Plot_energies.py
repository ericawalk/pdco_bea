from ase import atom , Atoms
from ase import neighborlist
from ase.io import read
from scipy import sparse
import numpy as np
from numpy import linalg as LA
import sys
import os
from os.path import join
import re
import pandas as pd
import math
import seaborn as sns

cwd = os.getcwd()
energies = [] 
arrangements = []
Al_NN = []
Al_dist = []

for root, dirs, files, in os.walk(cwd):
    for name in dirs:
        os.chdir(join(root, name))
        with open('OUTCAR') as f:
            content = f.readlines()
            rcontent = reversed(content)
        for line in rcontent:
            line = line.strip()
            if 'sigma' in line:
                nums = re.findall(r"[-+]?\d*\.\d+|\d+", line)
                num = float(nums[-1])
                break
            
        lattice = read('CONTCAR')
        length = len(lattice)
        Als = []
        count = 1
        cutOff = neighborlist.natural_cutoffs(lattice)
        neighborList = neighborlist.NeighborList(cutOff, self_interaction = False, bothways = True)
        neighborList.update(lattice)
        adj = neighborList.get_connectivity_matrix(sparse = False)

        for i in range(length):
            if lattice.symbols[i] == 'Al':
                Als.append(i)

        adj_m = LA.matrix_power(adj, count)
        while adj_m[Als[0], Als[1]] == 0:
            count +=1
            adj_m = LA.matrix_power(adj, count)
        with open('CONTCAR') as f:
            line1 = f.readline()
            lat_const = f.readline()
            xx = [float(x) for x in f.readline().split()] #Lattice vectors
            yy = [float(y) for y in f.readline().split()]
            zz = [float(z) for z in f.readline().split()]
            symbols = f.readline().split()
            numbers = [int(n) for n in f.readline().split()]
            direct = f.readline()
            total = sum(numbers)
            atoms_pos = np.empty((total, 3))
            for atom in range(total):
                ac = f.readline().split()
                atoms_pos[atom] = (float(ac[0]), float(ac[1]), float(ac[2]))
    
        a = LA.norm(xx) #Lattice parameters, angles in radians
        b = LA.norm(yy)
        c = LA.norm(zz)
        A = math.acos(np.dot(yy,zz)/b/c)
        B = math.acos(np.dot(xx,zz)/a/c)
        Y = math.acos(np.dot(xx,yy)/a/b)
        delt = atoms_pos[Als[0]] - atoms_pos[Als[1]]
        deltx1 = atoms_pos[Als[0]] - atoms_pos[Als[1]]
        delty1 = atoms_pos[Als[0]] - atoms_pos[Als[1]]
        deltz1 = atoms_pos[Als[0]] - atoms_pos[Als[1]]
        deltx_1 = atoms_pos[Als[0]] - atoms_pos[Als[1]]
        delty_1 = atoms_pos[Als[0]] - atoms_pos[Als[1]]
        deltz_1 = atoms_pos[Als[0]] - atoms_pos[Als[1]]
        deltx1[0] = delt[0] - 1
        delty1[1] = delt[1] - 1
        deltz1[2] = delt[2] - 1
        deltx_1[0] = delt[0] + 1
        delty_1[1] = delt[1] + 1
        deltz_1[2] = delt[2] + 1
        deltot = np.array([delt,deltx1,delty1,deltz1,deltx_1,delty_1,deltz_1])
        d = np.empty(7)
        k = 0
        for row in deltot:
            d[k] = math.sqrt(a**2*row[0]**2+b**2*row[1]**2+c**2*row[2]**2+2*b*c*math.cos(A)*row[1]*row[2]+2*c*a*math.cos(B)*row[2]*row[0]+2*a*b*math.cos(Y)*row[0]*row[1])
            k +=1
        d_min = np.amin(d)
        
        energies.append(num)
        arrangements.append(name)
        Al_NN.append(int((count/2)))
        Al_dist.append(d_min)
        E_min = np.amin(energies)
        E_standard = []
        for x in energies:
            E_standard.append(x-E_min)
        E_standard = np.array(E_standard)
os.chdir(cwd)
data = list(zip(Al_NN, Al_dist, arrangements, energies))
E_data = list(zip(Al_NN, Al_dist, E_standard))
ddata = pd.DataFrame(data, columns = ['neighbor away', 'Al-Al Distance (Angstroms)', 'arrangement', 'energy (eV)'])
E_ddata = pd.DataFrame(E_data, columns = ['Neighbor Away','Al-Al Distance (Angstroms)', 'Relative Energy (eV)'])
plot = ddata.plot(x = 'Al-Al Distance (Angstroms)', y= 'energy (eV)', kind = 'scatter')
plot2 = sns.pairplot(ddata, y_vars = ['energy (eV)'], x_vars = ['Al-Al Distance (Angstroms)'], hue = 'neighbor away',height = 4, aspect = 1)
plot3 = sns.pairplot(E_ddata, x_vars =['Al-Al Distance (Angstroms)'], y_vars = ['Relative Energy (eV)'], hue = 'Neighbor Away', height = 4, aspect = 1.15)
plot3.savefig('plot3.png', dpi = 300)
plot.figure.savefig('plot1.png')
plot2.savefig('plot2.png',dpi = 350)
#print(ddata)
ddata.to_csv('energies.csv')
