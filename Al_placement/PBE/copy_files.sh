#!/bin/sh
for d in */
do
	cp INCAR KPOINTS POTCAR slurm_run_vasp.sh $d
        #mv $d/POSCAR $d/POSCAR_org #renames POSCAR so it is not deleted in next line
        #mv $d/CONTCAR $d/POSCAR #Replaces POSCAR with CONTCAR, this will also delete previous POSCARs that are not renamed
	echo "$d complete"
done
