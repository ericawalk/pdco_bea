#!/bin/sh
for d in */
do
    cd $d
    sbatch run_vasp.sh
    echo "$d submitted"
    cd ../
done
