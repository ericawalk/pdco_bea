from ase import atom , Atoms
from ase.io import read
from ase.io import write
from ase.constraints import FixAtoms
from ase.io.vasp import write_vasp
import math
import os.path
import numpy as np
from numpy import linalg as LA
import os
import shutil
cwd = os.getcwd()
import os

for subdir, dirs, files in os.walk(cwd):
    for file in files:
        if file=='CONTCAR':
            struct = read(os.path.join(subdir,file))
            write('./xyz_CONTCARS/BEA' + subdir[-7:] + '.xyz', struct, format='xyz')
