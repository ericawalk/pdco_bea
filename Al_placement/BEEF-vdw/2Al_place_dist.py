from ase import atom , Atoms
from ase.io import read
from ase.io import write
from ase.constraints import FixAtoms
from ase.io.vasp import write_vasp
import math
import os.path
import numpy as np
from numpy import linalg as LA
import os
import shutil
cwd = os.getcwd()

lattice = read('POSCAR_BEA')
atoms = len(lattice)
total_Silicon = []
# finds all silicon atoms
for i in range(atoms):
    if lattice.symbols[i] == 'Si':
        total_Silicon.append(i)
# Get positions of all atoms and lattice parameters
with open('POSCAR_BEA') as f:
    line1 = f.readline()
    lat_const = f.readline()
    xx = [float(x) for x in f.readline().split()] #Lattice vectors
    yy = [float(y) for y in f.readline().split()]
    zz = [float(z) for z in f.readline().split()]
    symbols = f.readline().split()
    numbers = [int(n) for n in f.readline().split()]
    direct = f.readline()
    total = sum(numbers)
    atoms_pos = np.empty((total, 3))
    for atom in range(total):
        ac = f.readline().split()
        atoms_pos[atom] = (float(ac[0]), float(ac[1]), float(ac[2]))
a = LA.norm(xx) #Lattice parameters, angles in radians
b = LA.norm(yy)
c = LA.norm(zz)
A = math.acos(np.dot(yy,zz)/b/c)
B = math.acos(np.dot(xx,zz)/a/c)
Y = math.acos(np.dot(xx,yy)/a/b)

# choose individual silicons
total_Silicon = [128, 133, 136, 138, 141, 143, 144, 149, 152, 154, 157, 159, 170, 175, 176, 181, 184, 186]
#total_Silicon = [154, 159, 170, 175]
length = len(total_Silicon)
total_Silicon = np.array(total_Silicon)
d_mins = []
#Loops through all Si atoms and replaces each with Al, excluding doubles, Lowensteins rule is not followed
for l in range(length):
    for k in range(length):

        if l == k:
            continue
        # skips duplicates of inverse order (1-2 vs 2-1)
        if os.path.exists(cwd + '/' + str(total_Silicon[k]) + '-' + str(total_Silicon[l])):
            continue
        
        # skips duplicates that will yeild same Al-Al distance
        delt = atoms_pos[total_Silicon[l]] - atoms_pos[total_Silicon[k]]
        deltx1 = atoms_pos[total_Silicon[l]] - atoms_pos[total_Silicon[k]]
        delty1 = atoms_pos[total_Silicon[l]] - atoms_pos[total_Silicon[k]]
        deltz1 = atoms_pos[total_Silicon[l]] - atoms_pos[total_Silicon[k]]
        deltx_1 = atoms_pos[total_Silicon[l]] - atoms_pos[total_Silicon[k]]
        delty_1 = atoms_pos[total_Silicon[l]] - atoms_pos[total_Silicon[k]]
        deltz_1 = atoms_pos[total_Silicon[l]] - atoms_pos[total_Silicon[k]]
        deltx1[0] = delt[0] - 1
        delty1[1] = delt[1] - 1
        deltz1[2] = delt[2] - 1
        deltx_1[0] = delt[0] + 1
        delty_1[1] = delt[1] + 1
        deltz_1[2] = delt[2] + 1
        deltot = np.array([delt,deltx1,delty1,deltz1,deltx_1,delty_1,deltz_1])
        d = np.empty(7)
        C = 0
        for row in deltot:
            d[C] = math.sqrt(a**2*row[0]**2+b**2*row[1]**2+c**2*row[2]**2+2*b*c*math.cos(A)*row[1]*row[2]+2*c*a*math.cos(B)*row[2]*row[0]+2*a*b*math.cos(Y)*row[0]*row[1])
            C +=1
        d_min = np.amin(d)
        d_min = round(d_min,2)
        if d_min in d_mins:
            continue
        if not d_min in d_mins:
            d_mins.append(d_min)
        # replaces chosen Si atoms with Al
        lattice = read('POSCAR_BEA')
        lattice.symbols[total_Silicon[l]] = 'Al'
        lattice.symbols[total_Silicon[k]] = 'Al'
        
        #makes directory of each Al-Al case denoted by which Si was replaced
        if not os.path.exists(cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k])):
            os.mkdir(cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k]))

        #move to made directory from working directory of this script
        os.chdir(cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k]))

        #write POSCAR format of  Al-Al substituted zeolite framework
        write_vasp('POSCAR'.format(total_Silicon[l], total_Silicon[k]), lattice, label = '', direct=True, sort=True, symbol_count=None, long_format=True, vasp5=False)
        #edit POSCAR to add atom names --> vesta compatible
        f = open('POSCAR', 'r')
        contents = f.readlines()
        f.close
        contents.insert(5,'   Al    O   Si\n' )
        f = open('POSCAR','w')
        contents = ''.join(contents)
        f.write(contents)
        f.close()

        print("%s-%s is complete" % (total_Silicon[l], total_Silicon[k]), end='\r')  

        #copy INCAR, POTCAR, KPOINTS, and slurm script for each directory of Al-Al case
        #shutil.copy(cwd + '/' + 'INCAR', cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k]))
        #shutil.copy(cwd + '/' + 'POTCAR', cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k]))
        #shutil.copy(cwd + '/' + 'KPOINTS', cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k]))
        #shutil.copy(cwd + '/' + 'slurm_run_vasp.sh', cwd + '/' + str(total_Silicon[l]) + '-' + str(total_Silicon[k]))
        
        #move back to the working directory of this script
        os.chdir(cwd)
