from ase import Atoms
from ase.build import molecule
from ase.io import write,read
from ase.lattice.surface import bcc111
from ase.constraints import FixAtoms
from ase.lattice.surface import fcc100,add_adsorbate

# Generate the representation of the atoms in ASE
slab = bcc111('Fe', size=(3,2,10), vacuum=15.0)
mask = [atom.tag > 2 for atom in slab]
slab.set_constraint(FixAtoms(mask=mask))
write("POSCAR",slab)


