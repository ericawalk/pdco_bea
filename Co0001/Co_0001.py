from ase import Atoms
from ase.build import molecule
from ase.io import write,read
from ase.lattice.surface import fcc111
from ase.constraints import FixAtoms
from ase.lattice.surface import fcc100,add_adsorbate,hcp0001

# Generate the representation of the atoms in ASE
slab = hcp0001('Co', size=(3,4,4), vacuum=15.0)
mask = [atom.tag > 2 for atom in slab]
slab.set_constraint(FixAtoms(mask=mask))
write("POSCAR",slab)


