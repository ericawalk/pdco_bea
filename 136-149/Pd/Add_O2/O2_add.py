from ase.io import read
from ase.io.vasp import write_vasp
from ase.build import molecule
import numpy as np
from numpy import linalg as LA
import math
import os

zeo = read('CONTCAR_zeo')
zeo_l = len(zeo)
mol = read('CONTCAR_O2')
mol_l = len(mol)
Zeo = zeo + mol
C = [zeo_l]
mol_atoms = []
for i in range(mol_l):
    mol_atoms.append(zeo_l+i)
del mol_atoms[0]
ads = [.91,.6,.26] #fractional
########convert from fractional to cartesian####################################
with open('CONTCAR_zeo') as f:
    line1 = f.readline()
    lat_const = f.readline()
    xx = [float(x) for x in f.readline().split()] #Lattice vectors
    yy = [float(y) for y in f.readline().split()]
    zz = [float(z) for z in f.readline().split()]
    symbols = f.readline().split()
    numbers = [int(n) for n in f.readline().split()]
    direct = f.readline()
    total = sum(numbers)
    atoms_pos = np.empty((total, 3))
    for atom in range(total):
        ac = f.readline().split()
        atoms_pos[atom] = (float(ac[0]), float(ac[1]), float(ac[2]))
a = LA.norm(xx) #Lattice parameters, angles in radians
b = LA.norm(yy)
c = LA.norm(zz)
alpha = math.acos(np.dot(yy,zz)/b/c)
beta = math.acos(np.dot(xx,zz)/a/c)
gamma = math.acos(np.dot(xx,yy)/a/b)
cosa = np.cos(alpha)
sina = np.sin(alpha)
cosb = np.cos(beta)
sinb = np.sin(beta)
cosg = np.cos(gamma)
sing = np.sin(gamma)
volume = 1.0 - cosa**2.0 - cosb**2.0 - cosg**2.0 + 2.0 * cosa * cosb * cosg
volume = np.sqrt(volume)
r = np.zeros((3, 3))
r[0, 0] = a
r[0, 1] = b * cosg
r[0, 2] = c * cosb
r[1, 1] = b * sing
r[1, 2] = c * (cosa - cosb * cosg) / sing
r[2, 2] = c * volume / sing
cart_ads = np.matmul(r,ads)
################################################################################
L = len(mol_atoms)
diff = []
for k in range(L):
    diff.append(Zeo.get_distance(mol_atoms[k], C, vector = True))
diff = np.array(diff)

pos = Zeo.get_positions()
pos = np.array(pos)
pos[C] = np.array(cart_ads)
Zeo.set_positions(pos)
pos = Zeo.get_positions()
pos = np.array(pos)
for k in range(L):
    pos[mol_atoms[k]] = pos[C]-diff[k]
Zeo.set_positions(pos)
       
write_vasp('POSCAR', Zeo, label = '', direct=True, sort=False, symbol_count=None, long_format=True)
f = open('POSCAR', 'r')
contents = f.readlines()
f.close
f = open('POSCAR', 'r')
species = f.readline()
f.close
contents.insert(5,species)
f = open('POSCAR', 'w')
contents = ''.join(contents)
f.write(contents)
f.close
